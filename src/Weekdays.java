import java.util.Scanner;

public class Weekdays {

	private static int pointWeekDay() {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter the day");
		int day = scan.nextInt();

		return day;
	}

	public static void main(String[] args) {

		int day = pointWeekDay();
		String weekDay;
		String dayOfWeek;
		switch (day) {
		case 1:
			dayOfWeek = "Monday";
			weekDay = "st";
			break;
		case 2:
			dayOfWeek = "Tuesday";
			weekDay = "nd";
			break;
		case 3:
			dayOfWeek = "Wednesday";
			weekDay = "rd";
			break;
		case 4:
			dayOfWeek = "Thursday";
			weekDay = "th";
			break;
		case 5:
			dayOfWeek = "Friday";
			weekDay = "th";
			break;
		case 6:
			dayOfWeek = "Saturday";
			weekDay = "th";
			break;
		case 7:
			dayOfWeek = "Sunday";
			weekDay = "th";
			break;
		default:
			dayOfWeek = "impossible";
			weekDay = "th";
			break;
		}
		System.out.println(" The " + day + weekDay + " day of the week is " + dayOfWeek + ".");
	}
}