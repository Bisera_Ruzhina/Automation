import java.util.Scanner;

public class Vacantion {

	public static void main(String[] args) {
		String vacationType = askVacationType();
		int budget = askBudget();

		if (vacationType == "sea") {
			if (budget > 50) {
				System.out.println("Go outside Bulgaria");
			} else {
				System.out.println("Stay in Bulgaria");
			}
		} else {
			if (budget > 30) {
				System.out.println("Go outside Bulgaria");
			} else {
				System.out.println("Stay in Bulgaria");
			}
		}
	}

	// This method determine desired type of vacation.
	public static String askVacationType() {
		String ret = "";
		Scanner scan = new Scanner(System.in);

		while (!ret.equals("sea") && !ret.equals("mountain")) {
			System.out.println("Choose type of vacation: sea, mountain: ");
			ret = scan.next();
		}

		return ret;
	}

	// This method determine budget.
	public static int askBudget() {
		int ret;
		Scanner scan = new Scanner(System.in);

		System.out.println("Choose budget ");
		ret = scan.nextInt();

		return ret;
	}

}
