import java.util.*;

	public class LargestNo4loop {

	 public static void main(String[] args) {

	  List<Integer> intValues = new ArrayList<>();

	  intValues.add(64);
	  intValues.add(62);
	  intValues.add(25);
	  intValues.add(60);
	  intValues.add(19);
	  intValues.add(2);

	  Integer max = Collections.max(intValues);
	 
	  System.out.println("ArrayList max value : " + max);
	 }
	}
