import java.util.Scanner;

public class Work {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		System.out.println("Enter your age");
		int age = scan.nextInt();
		determine(age);

	}

	public static void determine(int age) {
		if (age >= 16)
			System.out.println("You are eligable to work");
		else
			System.out.println("You are not eligable to work");

	}

}
