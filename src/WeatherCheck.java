import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;


public class WeatherCheck {


    public static void main(String[] args) {
        System.setProperty("webriver.chrome.driver", "home/bisera/Downloads/chromedriver-linux64");
        WebDriver driver = new ChromeDriver();
        //Loading Meteoblue's website
        driver.get("https://www.meteoblue.com");
        //Accept the cookie settings
        WebElement cookies = driver.findElement(By.xpath("//input[@id='Accept and continue']"));
        cookies.click();
        //Click on search field
        WebElement search = driver.findElement(By.xpath("//input[@id='gls']"));
        search.click();
        //Fill Sofia
        search.sendKeys("Sofia");
        // Specify the timeout duration
        Duration timeout = Duration.ofSeconds(10);
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        // Define the WebElement for the specific dropdown item
        WebElement sofiaOption = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@class='loc active']//div[@class='locationname-inside'][normalize-space()='Sofia']")));
        // Click on the dropdown item
        sofiaOption.click();
        // Wait for a few seconds to see the result
        try {
            Thread.sleep(7000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Close the browser
        driver.quit();
    }
}




