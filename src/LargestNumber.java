
public class LargestNumber {

	public static void main(String[] args) {
		int[] numbers = { 1, 2, 3, 4, 25, 78, 20, 7894};
		int max = largest(numbers);

		System.out.println(max);
	}

	private static int largest(int[] numbers) {
		int max = numbers[0];
		for (int y = 1; y < numbers.length; y++)	
		{
		    if (numbers[y] > max)
		    max=numbers[y];
		}
		return max;
	}
}