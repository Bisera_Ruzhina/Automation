public class DuplicateArray {
    public static void main(String[] args) {
        int[] arr = new int[]{2, 5, 8, 7, 3, 8, 2};
        System.out.println("Duplicate elements in the array are: ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j])
                    System.out.println(arr[j]);

            }

        }
    }
}
