import java.util.Scanner;

public class Price {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the price");
		int price = scan.nextInt();
		System.out.println("Enter the quantity");
		int units = scan.nextInt();
		float discount = determineDiscount(units);
		System.out.println(units * price * discount + "$");
		float revenue = determineRevenue(units, price, discount);
	}

	public static float determineDiscount(int units) {
		float ret;
		if (units < 100) {
			System.out.println("There is no discount");
			ret = 0.00f;
		} else if (units < 120) {
			System.out.println("The discount is 15%");
			ret = 0.15f;
		} else {
			System.out.println("The discount is 20%");
			ret = 0.20f;
		}

		return ret;
	}

	public static float determineRevenue(int units, int price, float discount) {
		float revenue = units * price * (1 - discount);
		System.out.println("The revenue is:" + revenue + "$");

		return revenue;
	}

}
