import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.openqa.selenium.By.*;

public class Login {
    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.gecko.driver", "../../intellij/geckodriver");
        WebDriver dr = new FirefoxDriver();
        dr.get("https://qafour.profitstarsfps.com/");
        dr.manage().window().maximize();
        //click on username
        WebElement username = dr.findElement(id("signInName"));
        //type in  username field
        username.sendKeys("testuser128");
        // click on Password
        WebElement password = dr.findElement(id("password"));
        //Fill the field
        password.sendKeys("7$8,Qd;uBL");
        //Click on Sign in
        WebElement SignIn = dr.findElement(id("next"));
        SignIn.click();
    }
}
