public class Sum {

	public static void main(String[] args) {
	
		int[] numbers = {1,200,13,24};
		int sum = 0;
		for( int i : numbers) {
		    sum += i;
		}

		System.out.println(sum);
	}
}
