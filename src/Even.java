public class Even {

	public static void main(String[] args) {
		System.out.println(isEven(42));

	}
	
	public static boolean isEven(int num) {
		return num % 2 == 0;
	}
}
