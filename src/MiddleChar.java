
public class MiddleChar {

	public static void main(String[] args) {
		printMiddleCharacter("infinity");
	}

	public static void printMiddleCharacter(String str) {

		int len = str.length();
		if (len == 0) {
			return;
		}


		if (len % 2 == 0) {
			
			System.out.println(str.charAt(len / 2 - 1));
		}
		System.out.println(str.charAt(len / 2));

	}

}
