import java.util.LinkedList;


public class ReverseLinkedList
{
    //private class Datatype implements LinkedList<int> {}


    public static void main( String[] args )
    {
        LinkedList<Integer> original = generate_test_data();
        reverse_inplace( original );
        System.out.println( original );
    }


    private static LinkedList<Integer> reverse( LinkedList<Integer> original )
    {
        LinkedList<Integer> ret = new LinkedList<Integer>();
        for( int i = original.size(); i > 0; --i )
        {
            ret.add( original.get( i - 1 ) );
	}
        return ret;
    }


    private static void reverse_inplace( LinkedList<Integer> nums )
    {
        for( int i = 0; i < nums.size() / 2; ++i )
        {
            int other_index = nums.size() - i - 1;
            Integer tmp = nums.get(i);
            nums.set( i, nums.get(other_index) );
            nums.set( other_index, tmp );
	}
    }


    private static LinkedList<Integer> generate_test_data()
    {
        LinkedList<Integer> numbers = new LinkedList<>();
        numbers.add(3);
        numbers.add(42);
        numbers.add(667);
        numbers.add(9000);
        numbers.add(10000);
        return numbers;
    }

}
