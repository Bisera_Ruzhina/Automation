import java.util.Scanner;

public class TwoDMatrixJUI {

	public static void main(String[] args) {

		int[][] mat = askMatrix();
		TwoDmatrix.print(mat);
	}

	private static int askInt(String msg) {

		Scanner scan = new Scanner(System.in);
		System.out.println(msg);
		int day = scan.nextInt();

		return day;
	}

	public static int[][] askMatrix() {
		int rows = askInt("How many rows?");
		int cols = askInt("What about columns");
		int[][] mat = new int[rows][cols];
		fill(mat);
		return mat;
	}

	public static void fill(int[][] mat) {
		for (int row = 0; row < mat.length; row++) {
			for (int col = 0; col < mat[0].length; col++) {
				mat[row][col] = row * mat[0].length + col;
			}
		}
	}
}
