
public class Palindrome {

	public static void main(String[] args) {
		int[] numbers = { 7, 20, 77, 102, 101, 1331, 56742, 8 };

		for (int number : numbers) {
			System.out.println("Is number " + number + " a palindrome? " + isPalindrome(number));
		}
	}

	private static boolean isPalindrome(int number) {
		if (number == reverse(number)) {
			return true;
		}
		return false;
	}

	private static int reverse(int number) {
		int reversed = 0;

		while (number != 0) {
			reversed *= 10;
			reversed += number % 10;
			number /= 10;
		}

		return reversed;
	}

}
