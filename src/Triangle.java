import java.util.Scanner;

enum Type {
	  IMPOSSIBLE,
	  ACUTE,
	  RIGHTANGLED,
	  OBTUSE,
	  EQUILATERAL, //also acute
	  ISOSCELES,
	  MULTIFACETED
	  }

public class Triangle {

	public static void main(String[] args) {

		int[] angles = readAngles ();
		
		//System.out.println(angles[0], angles[1], angles[2]);
		System.out.println("the angles are: " + angles[0] + ", " + angles[1] + ", " + angles[2]);
		Type t = determine(angles);
		printResult(t);
	}
	
	private static int[] readAngles () { 
		int[] angles = new int[3];  
		Scanner scan = new Scanner(System.in);

		System.out.println("Enter first triangle angle:");
		angles[0] = scan.nextInt();
		System.out.println("Enter second triangle angle:");
		angles[1] = scan.nextInt();
		System.out.println("Enter third triangle angle:");
		angles[2] = scan.nextInt();

		return angles;
	}

	private static Type determine(int[] angles) {
		if (angles [0]+ angles [1]+ angles [2] != 180 || angles[0] == 0 || angles[1] == 0 || angles[2] == 0 )return Type.IMPOSSIBLE;
		else if(angles[0] < 90 && angles[1] < 90 && angles[2] < 90) return Type.ACUTE;
		else if (angles[0] == 90 || angles[1] == 90 || angles[2] == 90)return Type.RIGHTANGLED;
		else if (angles[0] > 90 || angles[1] > 90 || angles[2] > 90) return Type.OBTUSE;
		else if (angles[0] == 60 && angles[1] == 60 && angles[2] == 60 )return Type.EQUILATERAL;
		else if (angles[0] == angles[1] || angles[1] == angles[2] || angles[2] == angles[0])return Type.ISOSCELES;
		else return Type.MULTIFACETED;
	}
	
	private static void printResult(Type t) {
		if (t==Type.IMPOSSIBLE)
			System.out.println("impossible");
		else if (t == Type.ACUTE) {
			System.out.println("acute");
		} else if (t == Type.RIGHTANGLED) {
			System.out.println("rightangled");
		} else if (t == Type.OBTUSE) {
			System.out.println("obtuse");
		} else if (t == Type.EQUILATERAL) {
			System.out.println("equilateral");
		} else if (t == Type.ISOSCELES) {
			System.out.println("isosceles");
		} else if (t == Type.MULTIFACETED) {
			System.out.println("multifaceted");

		}
	}
}
